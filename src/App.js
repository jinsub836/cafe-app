import logo from './logo.svg';
import './App.css';
import {Route, Routes} from "react-router-dom";
import DefaultLayout from "./Layouts/defaultLayout";
import Page1 from "./pages/page1";
import MainPage from "./pages/MainPage";

function App() {
  return (
    <div className="App">
    <Routes>
      <Route path="/" element={<DefaultLayout> <Page1/></DefaultLayout>} />
    </Routes>
    </div>
  );
}

export default App;
