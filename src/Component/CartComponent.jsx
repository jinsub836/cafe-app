import React from "react";

const CartComponent = ({product , onRemove}) => (
    <div>
        <p>상품명 : {product.goodsName} <br/>
            가격 : {product.price * product.quantity} <br/>
            수량: {product.quantity}
        </p>
        <button onClick={() => {onRemove(product)}} > 삭제 </button>
    </div>
)

export default CartComponent