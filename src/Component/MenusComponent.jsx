import React from "react";

const MenusComponent = ({product , onClick} ) => (
   <div onClick={()=>onClick(product)}>
       <div><img src={product.image} alt={product.goodsName}/></div>
       <div>{product.goodsName}</div>
       <div>{product.price} 원 </div>
   </div>
)


export default MenusComponent;