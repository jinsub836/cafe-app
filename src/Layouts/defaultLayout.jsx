import React from "react";

const DefaultLayout = ({ children }) => {

    return(
        <>
            <div>
                <h2 className="layoutHeader">
                <img src='http://localhost:3000/assets/Logo.png' alt='logo' />
                </h2>
                <nav></nav>
            </div>
            <main>{children}</main>
            <footer> </footer>
        </>
    )
}

export default DefaultLayout