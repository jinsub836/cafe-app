import React, {useCallback, useEffect, useState} from 'react';
import MenusComponent from "../Component/MenusComponent";
import CartComponent from "../Component/CartComponent";

const Page1 = () => {
    const [products, setProducts] = useState([])
    const [oderList, setOderList] = useState([])

    useEffect(() => {
        fetch('http://localhost:3000/goodsMenu.json')
            .then(response => response.json())
            .then(data => setProducts(data))
    }, []);

    const onClick = useCallback(e => {
            setOderList((preCart) => {
                // 기존에 해당 이름의 아이템이 존재 하는지 확인하는 조건
                const existingItem = preCart.find((value) => value.goodsName === e.goodsName)
                // 기존에 아이템이 존재 한다면
                if(existingItem) {
                    return preCart.map((item) =>
                    item.goodsName === e.goodsName ?
                        // 기존 아이템이 존재한다가 참이라면 수량에 +1을 해준다.
                        {...item , quantity : item.quantity +1 } : item
                    )
                } else {
                    // 만약 새롭게 등록되는 아이템일 경우 수량 1로 하여 넣어준다.
                    return [...preCart, {...e , quantity : 1}]
                }
            })
        }, [])

    const onRemove = useCallback( e =>{
        console.log(oderList)
        setOderList((preCart) => {
            // 기존에 해당 이름의 아이템이 존재 하는지 확인하는 조건
            const existingItem = preCart.find((value) => value.goodsName === e.goodsName && value.quantity > 1)
            // 기존에 아이템이 존재 한다면
            if(existingItem) {
                return preCart.map((item) =>
                    item.goodsName === e.goodsName ?
                        // 기존 아이템이 존재한다가 참이라면 수량에 +1을 해준다.
                        {...item , quantity : item.quantity -1 } : item
                )
            } else {
                // 만약 새롭게 등록되는 아이템일 경우 수량 1로 하여 넣어준다.
                return preCart.filter((item) =>
                item.goodsName !== e.goodsName
                )
            }
        })
    },[])


    return (
        <div className="container">

            <div className="goods">
                {products.map(product =>
                    <div key={product.id}>
                        <MenusComponent product={product} onClick={onClick}/></div>
                )}

            </div>
            <div className="orderList">
                <div>
                    <div className="orderItems">
                        <h2>주문 목록 </h2>
                        {oderList.map(product =>
                            <div className='orderItem' key={product.id}>
                                <CartComponent product={product} onRemove={onRemove} />
                            </div>
                        )}
                    </div>
                </div>
                <div className="totalPrice">
                    <div className="totalPrice2">
                        합계 : {oderList.reduce((acc, curr) => acc + curr.price, 0)} 원
                    </div>
                </div>
            </div>
        </div>
    )

}

export default Page1